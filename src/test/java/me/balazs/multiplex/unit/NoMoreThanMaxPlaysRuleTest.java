package me.balazs.multiplex.unit;

import me.balazs.multiplex.controller.MovieController;
import me.balazs.multiplex.domain.Movie;
import me.balazs.multiplex.domain.Event;
import me.balazs.multiplex.util.validator.MaxPlaysRule;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class NoMoreThanMaxPlaysRuleTest {

    @InjectMocks
    MaxPlaysRule underTest = new MaxPlaysRule();

    @Mock MovieController movieController;
    @Mock Movie movie;
    @Mock Event event;

    @Before
    public void setUp(){
        when(movie.getMaxPlay()).thenReturn(3);
        when(event.getMovie()).thenReturn(movie);
    }

    @Test
    public void testValidateWhenMovieUnderTheMaximumPlays(){
        when(movieController.getAmountOfScreenings(movie)).thenReturn(2);

        underTest.validate(event);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateWhenMovieReachTheMaximumPlays(){
        when(movieController.getAmountOfScreenings(movie)).thenReturn(3);

        underTest.validate(event);
    }
}

package me.balazs.multiplex.unit;

import me.balazs.multiplex.controller.EventController;
import me.balazs.multiplex.domain.Movie;
import me.balazs.multiplex.domain.Event;
import me.balazs.multiplex.util.validator.ThreeMoviesSimultaneousRule;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDateTime;
import java.util.Arrays;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ThreeMoviesSimultaneousRuleTest {

    @InjectMocks
    ThreeMoviesSimultaneousRule underTest = new ThreeMoviesSimultaneousRule();

    @Mock
    EventController eventController;

    @Mock
    Movie movie;

    @Mock Event event1;
    @Mock Event event2;
    @Mock Event event3;
    @Mock Event event4;

    @Before
    public void setUp(){
        when(movie.getLength()).thenReturn(30);
        when(event1.getMovie()).thenReturn(movie);
        when(event1.getStart()).thenReturn(LocalDateTime.now());
        when(event2.getMovie()).thenReturn(movie);
        when(event2.getStart()).thenReturn(LocalDateTime.now());
        when(event3.getMovie()).thenReturn(movie);
        when(event3.getStart()).thenReturn(LocalDateTime.now());
        when(event4.getMovie()).thenReturn(movie);
        when(event4.getStart()).thenReturn(LocalDateTime.now());
        when(eventController.getByMovie(movie)).thenReturn(Arrays.asList(new Event[]{event1,event2,event3}));
    }

    @Test
    public void testValidate(){
        underTest.validate(event4);
    }

}

package me.balazs.multiplex.unit;

import me.balazs.multiplex.controller.DefaultEventController;
import me.balazs.multiplex.domain.Event;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.context.ContextConfiguration;

import java.time.LocalDateTime;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@ContextConfiguration
public class DefaultEventControllerTest {

    @InjectMocks
    DefaultEventController underTest = new DefaultEventController();

    @Mock Event event;

    @Before
    public void setUp(){
        when(event.getStart()).thenReturn(LocalDateTime.of(2000,01,01,01,01,01));
        when(event.getEnd()).thenReturn(LocalDateTime.of(2000,01,01,01,01,03));
    }

    @Test
    public void testIsDuringTheEventReturnTrueWhenDateIsTheStartDate(){
        assertTrue(underTest.isDuringTheEvent(event, event.getStart()));
    }

    @Test
    public void testIsDuringTheEventReturnTrueWhenDateIsBetweenTheStartAndEndDate(){
        assertTrue(underTest.isDuringTheEvent(event, event.getEnd().minusSeconds(1)));
    }

    @Test
    public void testIsDuringTheEventReturnTrueWhenDateIsTheEndDate(){
        assertTrue(underTest.isDuringTheEvent(event, event.getEnd()));
    }

    @Test
    public void testIsDuringTheEventReturnFalseWhenDateBeforeStart(){
        assertFalse(underTest.isDuringTheEvent(event, event.getStart().minusSeconds(1)));
    }

    @Test
    public void testIsDuringTheEventReturnFalseWhenDateAfterEnd(){
        assertFalse(underTest.isDuringTheEvent(event, event.getEnd().plusSeconds(1)));
    }

    @Test
    public void testIsDuringTheEventReturnTrueWhenOnlyFirstDateIsBetweenStartAndEndDate(){
        assertTrue(underTest.isDuringTheEvent(event, event.getEnd().minusSeconds(1), event.getEnd().plusSeconds(1)));
    }

    @Test
    public void testIsDuringTheEventReturnTrueWhenOnlySecondDateIsBetweenStartAndEndDate(){
        assertTrue(underTest.isDuringTheEvent(event, event.getStart().minusSeconds(1), event.getEnd().minusSeconds(1)));
    }

    @Test
    public void testIsDuringTheEventReturnTrueWhenBothDateIsBetweenStartAndEndDate(){
        assertTrue(underTest.isDuringTheEvent(event, event.getStart().plusSeconds(1), event.getEnd().minusSeconds(1)));
    }

    @Test
    public void testIsDuringTheEventReturnTrueWhenFirstDateBeforeStartAndSecondDateAfterEnd(){
        assertTrue(underTest.isDuringTheEvent(event, event.getStart().plusSeconds(1), event.getEnd().minusSeconds(1)));
    }

    @Test
    public void testIsDuringTheEventReturnFalseWhenBothDateBeforeTheEvent(){
        assertFalse(underTest.isDuringTheEvent(event, event.getStart().minusSeconds(2), event.getStart().minusSeconds(1)));
    }

}

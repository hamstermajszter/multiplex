package me.balazs.multiplex.configuration;

import com.neovisionaries.i18n.CountryCode;
import me.balazs.multiplex.controller.AuditoriumController;
import me.balazs.multiplex.controller.DefaultAuditoriumController;
import me.balazs.multiplex.domain.Auditorium;
import me.balazs.multiplex.domain.Event;
import me.balazs.multiplex.domain.Movie;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;

@Configuration
public class AuditoriumControllerTestConfiguration {

    @Bean
    public AuditoriumController auditoriumController(){
        return new DefaultAuditoriumController();
    }

    @Bean
    public Movie terminator(){ return Movie.builder().title("Terminator").director("Cameron").isDubbed(Boolean.FALSE).length(132).synopsis("Ez egy jó film").maxPlay(2).ageCategory(3).origin(CountryCode.US).build(); }
    @Bean
    public Auditorium petofi(){
        return Auditorium.builder().name("Petofi").columnSize(2).rowSize(3).build();
    }
    @Bean
    public Auditorium arany(){
        return Auditorium.builder().name("Arany").columnSize(4).rowSize(4).build();
    }


    @Bean
    public Event terminatorInPetofi(){
        return Event.builder().auditorium(petofi()).movie(terminator()).start(LocalDateTime.of(2016,10,12,10,24)).build();
    }
    @Bean
    public Event terminatorInArany(){
        return Event.builder().auditorium(arany()).movie(terminator()).start(LocalDateTime.of(2016,10,12,10,24)).build();
    }
}

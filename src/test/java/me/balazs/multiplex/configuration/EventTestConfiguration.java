package me.balazs.multiplex.configuration;

import com.neovisionaries.i18n.CountryCode;
import me.balazs.multiplex.domain.Auditorium;
import me.balazs.multiplex.domain.Event;
import me.balazs.multiplex.domain.Movie;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;

@Configuration
public class EventTestConfiguration {

    @Bean
    public Movie terminator(){
        return Movie.builder().title("Terminator").director("Cameron").isDubbed(Boolean.FALSE).length(132).synopsis("Ez egy jó film").maxPlay(2).ageCategory(3).origin(CountryCode.US).build();
    }

    @Bean
    public Auditorium petofi(){
        return Auditorium.builder().name("Petofi").columnSize(2).rowSize(3).build();
    }
    @Bean
    public Auditorium arany(){
        return Auditorium.builder().name("Arany").columnSize(4).rowSize(4).build();
    }
    @Bean
    public Auditorium ady(){
        return Auditorium.builder().name("ady").columnSize(2).rowSize(3).build();
    }
    @Bean
    public Auditorium jozsef(){
        return Auditorium.builder().name("jozsef").columnSize(4).rowSize(4).build();
    }


    @Bean
    public Event event1(){
        return Event.builder().auditorium(petofi()).movie(terminator()).start(LocalDateTime.now()).build();
    }
    @Bean
    public Event event2(){
        return Event.builder().auditorium(arany()).movie(terminator()).start(LocalDateTime.now()).build();
    }
    @Bean
    public Event event3(){
        return Event.builder().auditorium(ady()).movie(terminator()).start(LocalDateTime.now()).build();
    }


}

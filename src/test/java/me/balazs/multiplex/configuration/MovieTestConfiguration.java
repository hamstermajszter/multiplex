package me.balazs.multiplex.configuration;

import com.neovisionaries.i18n.CountryCode;
import me.balazs.multiplex.controller.MovieController;
import me.balazs.multiplex.controller.DefaultMovieController;
import me.balazs.multiplex.domain.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;

@Configuration
public class MovieTestConfiguration {

    @Bean
    public MovieController movieController() {
        return new DefaultMovieController();
    }

    @Bean
    public Movie terminator(){
        return Movie.builder().title("Terminator").director("Cameron").isDubbed(Boolean.FALSE).length(132).synopsis("Ez egy jó film").maxPlay(2).ageCategory(3).origin(CountryCode.US).build();
    }

    @Bean
    public Auditorium petofi(){
        return Auditorium.builder().name("Petofi").columnSize(2).rowSize(3).build();
    }
    @Bean
    public Auditorium arany(){
        return Auditorium.builder().name("Arany").columnSize(4).rowSize(4).build();
    }

    @Bean
    public Event terminatorInPetofi(){
        return Event.builder().auditorium(petofi()).movie(terminator()).start(LocalDateTime.now()).build();
    }
    @Bean
    public Event terminatorInArany(){
        return Event.builder().auditorium(arany()).movie(terminator()).start(LocalDateTime.now()).build();
    }

}

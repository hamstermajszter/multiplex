package me.balazs.multiplex.integration;

import me.balazs.multiplex.configuration.AuditoriumControllerTestConfiguration;
import me.balazs.multiplex.configuration.CommonTestConfiguration;
import me.balazs.multiplex.configuration.MovieTestConfiguration;
import me.balazs.multiplex.controller.AuditoriumController;
import me.balazs.multiplex.domain.Auditorium;
import me.balazs.multiplex.domain.Event;
import me.balazs.multiplex.domain.Movie;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.ApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {AuditoriumControllerTestConfiguration.class, CommonTestConfiguration.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class AuditoriumControllerIntegrationTest {

    @Autowired private TestEntityManager entityManager;
    @Autowired private ApplicationContext context;
    @Autowired private AuditoriumController auditoriumController;

    @Autowired private Movie terminator;

    @Value("#{petofi}") private Auditorium petofi;
    @Value("#{arany}") private Auditorium arany;

    @Value("#{terminatorInPetofi}") private Event terminatorInPetofi;
    @Value("#{terminatorInArany}") private Event terminatorInArany;

    @Before
    public void setUp(){
        entityManager.persist(terminator);
        entityManager.persist(petofi);
        entityManager.persist(arany);
        entityManager.persist(terminatorInPetofi);
        entityManager.persist(terminatorInArany);
    }

    @Test
    public void testIsAvailableWhenAuditoriumIsAvailable(){
        LocalDateTime dateTime = terminatorInPetofi.getStart().minusDays(5);
        assertThat(auditoriumController.isAvailable(petofi, dateTime, dateTime.plusMinutes(180))).isTrue();
    }

    @Test
    public void testIsAvailableWhenStartIsDuringTheEvent(){
        LocalDateTime dateTime = terminatorInPetofi.getStart().plusMinutes(5);
        assertThat(auditoriumController.isAvailable(petofi, dateTime, dateTime.plusMinutes(380))).isFalse();
    }

    @Test
    public void testIsAvailableWhenStartIsDuringTheCleanTime(){
        LocalDateTime dateTime = terminatorInPetofi.getEnd().minusMinutes(5);
        assertThat(auditoriumController.isAvailable(petofi, dateTime, dateTime.plusMinutes(380))).isFalse();
    }

    @Test
    public void testIsAvailableWhenEndIsDuringTheEvent(){
        LocalDateTime dateTime = terminatorInPetofi.getStart().plusMinutes(5);
        assertThat(auditoriumController.isAvailable(petofi, dateTime.minusMinutes(180), dateTime)).isFalse();
    }

    @Test
    public void testIsAvailaleWhenStartBeforeTheEvenetAndEndAfterTheEvenet(){
        LocalDateTime startTime = terminatorInPetofi.getStart().minusMinutes(5);
        LocalDateTime endTime = terminatorInPetofi.getEnd().plusMinutes(5);
        assertThat(auditoriumController.isAvailable(petofi, startTime, endTime)).isFalse();
    }
}

package me.balazs.multiplex.integration;

import me.balazs.multiplex.configuration.CommonTestConfiguration;
import me.balazs.multiplex.configuration.MovieTestConfiguration;
import me.balazs.multiplex.controller.MovieController;
import me.balazs.multiplex.domain.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.context.ApplicationContext;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Java6Assertions.assertThat;

@DataJpaTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {MovieTestConfiguration.class, CommonTestConfiguration.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class MovieControllerIntegrationTest{

    @Autowired private TestEntityManager entityManager;
    @Autowired private MovieController movieController;
    @Autowired private Movie terminator;

    @Value("#{petofi}") private Auditorium petofi;
    @Value("#{arany}") private Auditorium arany;

    @Value("#{terminatorInPetofi}") private Event terminatorInPetofi;
    @Value("#{terminatorInArany}") private Event terminatorInArany;

    @Autowired private ApplicationContext context;

    @Before
    public void setUp(){
        entityManager.persist(terminator);
        entityManager.persist(petofi);
        entityManager.persist(arany);
        entityManager.persist(terminatorInPetofi);
        entityManager.persist(terminatorInArany);
    }

    @Test
    public void testGetAmountOfSoldTicketsWhenMultipleAuditoriums(){
        entityManager.persist(Ticket.builder().event(terminatorInPetofi).seatColumn(1).seatRow(1).ticketStatus(TicketStatus.BOUGHT).build());
        entityManager.persist(Ticket.builder().event(terminatorInArany).seatColumn(1).seatRow(1).ticketStatus(TicketStatus.BOUGHT).build());

        assertThat(movieController.getAmountOfSoldTickets(terminator)).isEqualTo(2);
    }

    @Test
    public void testGetAmountOfSoldTicketsWhenOnlyBoughtTickets(){
        entityManager.persist(Ticket.builder().event(terminatorInPetofi).seatColumn(2).seatRow(1).ticketStatus(TicketStatus.BOUGHT).build());
        entityManager.persist(Ticket.builder().event(terminatorInPetofi).seatColumn(2).seatRow(1).ticketStatus(TicketStatus.BOUGHT).build());

        assertThat(movieController.getAmountOfSoldTickets(terminator)).isEqualTo(2);
    }

    @Test
    public void testGetAmountOfSoldTicketsWhenHaveReservedTicket(){
        entityManager.persist(Ticket.builder().event(terminatorInPetofi).seatColumn(2).seatRow(1).ticketStatus(TicketStatus.RESERVED).build());
        entityManager.persist(Ticket.builder().event(terminatorInPetofi).seatColumn(2).seatRow(1).ticketStatus(TicketStatus.BOUGHT).build());

        assertThat(movieController.getAmountOfSoldTickets(terminator)).isEqualTo(1);
    }

    @Test
    public void testGetAmountOfSoldTicketsWhenNoBoughtTicket(){
        entityManager.persist(Ticket.builder().event(terminatorInPetofi).seatColumn(2).seatRow(1).ticketStatus(TicketStatus.RESERVED).build());

        assertThat(movieController.getAmountOfSoldTickets(terminator)).isEqualTo(0);
    }

}

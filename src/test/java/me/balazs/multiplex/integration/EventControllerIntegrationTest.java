package me.balazs.multiplex.integration;

import me.balazs.multiplex.configuration.CommonTestConfiguration;
import me.balazs.multiplex.configuration.MovieTestConfiguration;
import me.balazs.multiplex.controller.EventController;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@DataJpaTest
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {MovieTestConfiguration.class, CommonTestConfiguration.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class EventControllerIntegrationTest {

    @Autowired private TestEntityManager entityManager;
    @Autowired private EventController eventController;


}

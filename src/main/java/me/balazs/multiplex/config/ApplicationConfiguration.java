package me.balazs.multiplex.config;

import me.balazs.multiplex.controller.*;
import me.balazs.multiplex.util.*;
import me.balazs.multiplex.util.validator.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfiguration {

    @Bean
    public AuditoriumController auditoriumController() { return new DefaultAuditoriumController(); }

    @Bean
    public EventController eventController() { return new DefaultEventController(); }

    @Bean
    public MovieController movieController() { return new DefaultMovieController(); }

    @Bean
    public TicketController ticketController() { return new DefaultTicketController(); }

    @Bean
    public Validator eventValidator() {
        Validator validator = new Validator();
        validator.addRule(ageCategoryRule());
        validator.addRule(auditoriumIsFreeRule());
        validator.addRule(maxPlaysRule());
        validator.addRule(threeMoviesSimultant());
        return validator;
    }

    @Bean
    public AuditoriumIsFreeRule auditoriumIsFreeRule() { return new AuditoriumIsFreeRule(); }

    @Bean
    public AgeCategoryRule ageCategoryRule() { return new AgeCategoryRule(); }

    @Bean
    public MaxPlaysRule maxPlaysRule() { return new MaxPlaysRule(); }

    @Bean
    public ThreeMoviesSimultaneousRule threeMoviesSimultant() {
        return new ThreeMoviesSimultaneousRule();
    }

    @Bean
    public Validator ticketValidator() {
        Validator validator = new Validator();
        validator.addRule(seatAlreadyTakenRule());
        return validator;
    }

    @Bean
    public SeatAlreadyTakenRule seatAlreadyTakenRule() {
        return new SeatAlreadyTakenRule();
    }

    @Bean
    public Populator populator() { return new Populator(); }

}

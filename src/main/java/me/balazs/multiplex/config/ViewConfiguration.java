package me.balazs.multiplex.config;

import me.balazs.multiplex.view.*;
import me.balazs.multiplex.view.model.AuditoriumsTableModel;
import me.balazs.multiplex.view.model.EventsTableModel;
import me.balazs.multiplex.view.model.MoviesTableModel;
import me.balazs.multiplex.view.model.TicketTableModel;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.swing.*;
import java.awt.*;

@Configuration
class ViewConfiguration {

    @Bean
    public JFrame frame() {
        JFrame frame = new JFrame("MainView");
        frame.setSize(new Dimension(900, 500));
        frame.setContentPane(main().getMainPanel());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        return frame;
    }

    @Bean
    public MainView main(){ return new MainView(); }
}

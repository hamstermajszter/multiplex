package me.balazs.multiplex.view;

import me.balazs.multiplex.util.Populator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.*;

@Component
public class MainView {


    private JPanel mainPanel;
    private JButton showMovies;
    private JButton showAuditoriums;
    private JButton showEvents;
    private JButton showTickets;
    private JPanel content;
    private JButton populatorButton;

    @Autowired private MoviesView moviesView;
    @Autowired private AuditoriumsView auditoriums;
    @Autowired private EventsView eventsView;
    @Autowired private TicketsView ticketsView;

    @Autowired private Populator populator;

    public MainView() {
        showMovies.addActionListener(e -> changeInnerPanel(moviesView.getInnerPanel()));
        showAuditoriums.addActionListener(e -> changeInnerPanel(auditoriums.getInnerPanel()));
        showEvents.addActionListener(e -> changeInnerPanel(eventsView.getInnerPanel()));
        showTickets.addActionListener(e -> changeInnerPanel(ticketsView.getInnerPanel()));
        populatorButton.addActionListener(e -> populator.populate());
    }

    private void changeInnerPanel(JPanel panel){
        content.removeAll();
        content.add(panel);
        content.revalidate();
        content.repaint();
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }

}

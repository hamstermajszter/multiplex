package me.balazs.multiplex.view;

import me.balazs.multiplex.controller.TicketController;
import me.balazs.multiplex.domain.Ticket;
import me.balazs.multiplex.domain.TicketStatus;
import me.balazs.multiplex.view.model.TicketTableModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;

@Component
public class TicketsView {
    private JPanel innerPanel;
    private JTable table;
    private JButton addNew;
    private JTextArea movieArea;
    private JTextArea auditoriumArea;
    private JTextArea dateArea;
    private JTextArea rowArea;
    private JTextArea columnArea;
    private JTextArea statusArea;
    private JButton buyButton;
    private JButton deleteButton;

    @Autowired private TicketDialog newTicketDialog;
    @Autowired private TicketTableModel ticketTableModel;
    @Autowired private TicketController ticketController;

    @PostConstruct
    private void init() {
        setUpButtons();
        setUpTable();
    }

    private void enableModificationButton(boolean enable) {
        buyButton.setEnabled(enable);
        deleteButton.setEnabled(enable);
    }

    private void setUpTable() {
        table.setModel(ticketTableModel);
        table.getSelectionModel().addListSelectionListener(e -> {
            Ticket ticket = ticketTableModel.getValueAt(table.getSelectedRow());
            fillSidebar(ticket);
        });
    }

    private void setUpButtons() {
        addNew.addActionListener(e -> newTicketDialog.display());
        buyButton.addActionListener(e -> {
            Ticket ticket = ticketTableModel.getValueAt(table.getSelectedRow());
            ticketController.sellTicket(ticket);
            fillSidebar(ticket);
        });
        deleteButton.addActionListener(e -> {
            Ticket ticket = ticketTableModel.getValueAt(table.getSelectedRow());
            ticketController.deleteTicket(ticket);
            ticketTableModel.fireTableDataChanged();
            fillSidebar(ticket);
        });
    }

    private void fillSidebar(Ticket ticket) {
        if(ticket != null) {
            movieArea.setText(ticket.getMovie().getTitle());
            auditoriumArea.setText(ticket.getAudotorium().getName());
            dateArea.setText(ticket.getStart().toString());
            rowArea.setText(ticket.getSeatRow().toString());
            columnArea.setText(ticket.getSeatColumn().toString());
            statusArea.setText(ticket.getTicketStatus() == TicketStatus.RESERVED ? "Foglalt" : "Megvásárolt");
            if(ticket.getTicketStatus() == TicketStatus.BOUGHT) {
                enableModificationButton(false);
            } else {
                enableModificationButton(true);
            }
        }
    }

    JPanel getInnerPanel() {
        ticketTableModel.fireTableDataChanged();
        return innerPanel;
    }
}

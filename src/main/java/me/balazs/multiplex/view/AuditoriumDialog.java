package me.balazs.multiplex.view;

import me.balazs.multiplex.controller.AuditoriumController;
import me.balazs.multiplex.domain.Auditorium;
import me.balazs.multiplex.view.model.AuditoriumsTableModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionSystemException;

import javax.swing.*;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.stream.Collectors;

@Component
public class AuditoriumDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField nameInput;
    private JSpinner rowInput;
    private JSpinner columnInput;

    private Auditorium auditorium;
    private final AuditoriumController auditoriumController;
    private final AuditoriumsTableModel auditoriumsTableModel;
    private final ErrorDialog errorDialog;

    @Autowired
    public AuditoriumDialog(ErrorDialog errorDialog, AuditoriumController auditoriumController, AuditoriumsTableModel auditoriumsTableModel) {
        setContentPane(contentPane);
        setUpButtons();
        this.errorDialog = errorDialog;
        this.auditoriumController = auditoriumController;
        this.auditoriumsTableModel = auditoriumsTableModel;
    }

    void display(Auditorium auditorium){
        this.auditorium = auditorium;
        setUpFields(auditorium);
        showIt(auditorium.getName());
    }

    void display(){
        this.auditorium = new Auditorium();
        setUpFields(null);
        showIt("Új vetítőterem");
    }

    private void showIt(String title) {
        setTitle(title);
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    private void setUpButtons() {
        buttonOK.addActionListener(e -> {
            try {
                onOK();
            } catch (ConstraintViolationException violation) {
                errorDialog.showIt(violation.getConstraintViolations().stream().map(ConstraintViolation::getMessage).collect(Collectors.joining(", ")));
            } catch (TransactionSystemException exception) {
                ConstraintViolationException violation = (ConstraintViolationException)exception.getCause().getCause();
                errorDialog.showIt(violation.getConstraintViolations().stream().map(ConstraintViolation::getMessage).collect(Collectors.joining(", ")));
            }
        });
        buttonCancel.addActionListener(e -> onCancel());
    }

    private void onOK() {
        auditorium.setName(nameInput.getText());
        auditorium.setRowSize((Integer) rowInput.getValue());
        auditorium.setColumnSize((Integer) columnInput.getValue());
        auditoriumController.add(auditorium);
        auditoriumsTableModel.fireTableDataChanged();
        dispose();
    }

    private void setUpFields(Auditorium auditorium) {
        if(auditorium == null){
            nameInput.setText("");
            rowInput.setValue(0);
            columnInput.setValue(0);
        } else {
            nameInput.setText(auditorium.getName());
            rowInput.setValue(auditorium.getRowSize());
            columnInput.setValue(auditorium.getColumnSize());
        }
    }

    private void onCancel() { dispose(); }



}

package me.balazs.multiplex.view;

import me.balazs.multiplex.controller.AuditoriumController;
import me.balazs.multiplex.controller.EventController;
import me.balazs.multiplex.controller.MovieController;
import me.balazs.multiplex.domain.Auditorium;
import me.balazs.multiplex.domain.Movie;
import me.balazs.multiplex.view.model.EventsTableModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.TableRowSorter;

@Component
public class EventsView {
    private JPanel innerPanel;
    private JButton addNew;
    private JButton editButton;
    private JButton deleteButton;
    private JTable table;
    private JTextField movieFilter;
    private JTextField auditoriumFilter;

    @Autowired private EventsTableModel eventsTableModel;
    @Autowired private EventController eventController;
    @Autowired private MovieController movieController;
    @Autowired private AuditoriumController auditoriumController;
    @Autowired private EventDialog eventDialog;
    @Autowired private ErrorDialog errorDialog;

    @PostConstruct
    public void init(){
        setUpButtons();
        setUpTable();
        setUpFilters();
    }

    private void setUpFilters() {
        movieFilter.getDocument().addDocumentListener(
                new DocumentListener() {
                    @Override
                    public void insertUpdate(DocumentEvent e) {
                        newFilter();
                    }

                    @Override
                    public void removeUpdate(DocumentEvent e) {
                        newFilter();
                    }

                    @Override
                    public void changedUpdate(DocumentEvent e) {
                        newFilter();
                    }
                }
        );
        auditoriumFilter.getDocument().addDocumentListener(
                new DocumentListener() {
                    @Override
                    public void insertUpdate(DocumentEvent e) {
                        newFilter();
                    }

                    @Override
                    public void removeUpdate(DocumentEvent e) {
                        newFilter();
                    }

                    @Override
                    public void changedUpdate(DocumentEvent e) {
                        newFilter();
                    }
                }
        );
    }

    private void newFilter() {
        TableRowSorter<EventsTableModel> sorter = new TableRowSorter<>(eventsTableModel);
        sorter.setRowFilter(RowFilter.regexFilter(movieFilter.getText(), 0));
        sorter.setRowFilter(RowFilter.regexFilter(auditoriumFilter.getText(), 1));
        table.setRowSorter(sorter);
    }

    private void enableModificationButtons(boolean enable) {
        editButton.setEnabled(enable);
        deleteButton.setEnabled(enable);
    }

    private void setUpTable() {
        table.setModel(eventsTableModel);
        table.getSelectionModel().addListSelectionListener(e -> enableModificationButtons(true));
    }

    private void setUpButtons() {
        addNew.addActionListener(e -> eventDialog.display());
        deleteButton.addActionListener(e -> {
            try {
                eventController.delete(eventsTableModel.getValueAt(table.getSelectedRow()).getId());
            } catch (DataIntegrityViolationException exception) {
                errorDialog.showIt("A vetítés nem törölhető, mert már foglaltak rá jegyet!");
            }
            eventsTableModel.fireTableDataChanged();
        });
        editButton.addActionListener(e -> eventDialog.display(eventsTableModel.getValueAt(table.getSelectedRow())));
        enableModificationButtons(false);
    }

    JPanel getInnerPanel() {
        eventsTableModel.fireTableDataChanged();
        return innerPanel;
    }
}

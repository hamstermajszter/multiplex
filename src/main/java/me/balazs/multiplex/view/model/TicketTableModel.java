package me.balazs.multiplex.view.model;

import me.balazs.multiplex.controller.TicketController;
import me.balazs.multiplex.domain.Ticket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.table.AbstractTableModel;
import java.util.Arrays;
import java.util.List;

@Component
public class TicketTableModel extends AbstractTableModel {

    private List<Ticket> tickets;
    private List<String> header;

    @Autowired TicketController ticketController;

    @PostConstruct
    private void init() {
        tickets = ticketController.getAll();
        header = Arrays.asList("Film", "Terem", "Sor", "Oszlop", "Időpont");
    }

    @Override
    public int getRowCount() {
        return tickets.size();
    }

    @Override
    public int getColumnCount() {
        return header.size();
    }

    @Override
    public String getColumnName(int column) {
        return header.get(column);
    }

    public Ticket getValueAt(int rowIndex) {
        if(rowIndex>=0) {
            return tickets.get(rowIndex);
        } else {
            return null;
        }
    }


    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Ticket ticket = tickets.get(rowIndex);
        switch (columnIndex){
            case 0:
                return ticket.getMovie().getTitle();
            case 1:
                return ticket.getAudotorium().getName();
            case 2:
                return ticket.getSeatRow();
            case 3:
                return ticket.getSeatColumn();
            case 4:
                return ticket.getEvent().getStart();
            default:
                return null;
        }
    }

    @Override
    public void fireTableDataChanged() {
        tickets = ticketController.getAll();
        super.fireTableDataChanged();
    }
}

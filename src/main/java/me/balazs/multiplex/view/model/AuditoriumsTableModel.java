package me.balazs.multiplex.view.model;

import me.balazs.multiplex.controller.AuditoriumController;
import me.balazs.multiplex.domain.Auditorium;
import me.balazs.multiplex.domain.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.table.AbstractTableModel;
import java.util.Arrays;
import java.util.List;

@Component
public class AuditoriumsTableModel extends AbstractTableModel{

    List<Auditorium> auditoriums;
    List<String> header;

    @Autowired AuditoriumController auditoriumController;

    @PostConstruct
    private void init(){
        auditoriums = auditoriumController.getAll();
        header = Arrays.asList("Név", "Sor", "Oszlop");
    }

    @Override
    public int getRowCount() {
        return auditoriums.size();
    }

    @Override
    public int getColumnCount() {
        return header.size();
    }

    @Override
    public String getColumnName(int column) {
        return header.get(column);
    }

    @Override
    public String getValueAt(int rowIndex, int columnIndex) {

        Auditorium auditorium = auditoriums.get(rowIndex);
        switch (columnIndex){
            case 0:
                return auditorium.getName();
            case 1:
                return String.valueOf(auditorium.getRowSize());
            case 2:
                return String.valueOf(auditorium.getColumnSize());
            default:
                return null;
        }
    }

    public Auditorium getValueAt(int rowIndex){
        if(rowIndex>=0) {
            return auditoriums.get(rowIndex);
        } else {
            return null;
        }
    }

    @Override
    public void fireTableDataChanged() {
        auditoriums = auditoriumController.getAll();
        super.fireTableDataChanged();
    }
}

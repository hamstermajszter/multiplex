package me.balazs.multiplex.view.model;

import me.balazs.multiplex.controller.MovieController;
import me.balazs.multiplex.domain.Movie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.table.AbstractTableModel;
import java.util.Arrays;
import java.util.List;

@Component
public class MoviesTableModel extends AbstractTableModel{

    private List<Movie> movies;
    private List<String> header;

    @Autowired
    private MovieController movieController;

    @PostConstruct
    private void init(){
        movies = movieController.getAll();
        header = Arrays.asList("Cím", "Rendező", "Eladott jegyek");
    }

    @Override
    public int getRowCount() { return movies.size(); }

    @Override
    public int getColumnCount() { return header.size(); }

    @Override
    public String getColumnName(int column) { return header.get(column); }

    @Override
    public void fireTableDataChanged() {
        movies = movieController.getAll();
        super.fireTableDataChanged();
    }

    public Movie getValueAt(int rowIndex){
        if(rowIndex>=0) {
            return movies.get(rowIndex);
        } else {
            return null;
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Movie movie = movies.get(rowIndex);
        switch (columnIndex){
            case 0:
                return movie.getTitle();
            case 1:
                return movie.getDirector();
            case 2:
                return movieController.getAmountOfSoldTickets(movie);
            default:
                return null;
        }
    }
}

package me.balazs.multiplex.view.model;

import me.balazs.multiplex.controller.EventController;
import me.balazs.multiplex.domain.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.table.AbstractTableModel;
import java.util.Arrays;
import java.util.List;

@Component
public class EventsTableModel extends AbstractTableModel{

    List<Event> events;
    List<String> header;

    @Autowired EventController eventController;

    @PostConstruct
    private void init() {
        events = eventController.getAll();
        header = Arrays.asList("Film", "Terem", "Dátum", "Szabad helyek");
    }

    @Override
    public int getRowCount() {
        return events.size();
    }

    @Override
    public int getColumnCount() {
        return header.size();
    }

    @Override
    public String getColumnName(int column) {
        return header.get(column);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Event event = events.get(rowIndex);
        switch (columnIndex){
            case 0:
                return event.getMovie().getTitle();
            case 1:
                return event.getAuditorium().getName();
            case 2:
                return event.getStart();
            case 3:
                return eventController.getAmountOfFreeSeats(event);
            default:
                return null;
        }
    }

    public Event getValueAt(int rowIndex){
        if(rowIndex>=0) {
            return events.get(rowIndex);
        } else {
            return null;
        }
    }

    @Override
    public void fireTableDataChanged() {
        events = eventController.getAll();
        super.fireTableDataChanged();
    }
}

package me.balazs.multiplex.view;

import me.balazs.multiplex.controller.AuditoriumController;
import me.balazs.multiplex.controller.EventController;
import me.balazs.multiplex.controller.MovieController;
import me.balazs.multiplex.domain.Auditorium;
import me.balazs.multiplex.domain.Event;
import me.balazs.multiplex.domain.Movie;
import me.balazs.multiplex.view.model.EventsTableModel;
import org.jdatepicker.DateModel;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionSystemException;

import javax.swing.*;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Properties;
import java.util.stream.Collectors;

@Component
public class EventDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JComboBox<Movie> movieInput;
    private JComboBox<Auditorium> auditoriumInput;
    private JDatePickerImpl datePicker;
    private JSpinner hourInput;
    private JSpinner minuteInput;

    private Event event;
    private final MovieController movieController;
    private final AuditoriumController auditoriumController;
    private final EventController eventController;
    private final EventsTableModel eventsTableModel;
    private final ErrorDialog errorDialog;

    @Autowired
    public EventDialog(MovieController movieController, AuditoriumController auditoriumController, EventController eventController, ErrorDialog errorDialog, EventsTableModel eventsTableModel) {
        setContentPane(contentPane);
        setUpButtons();
        this.movieController = movieController;
        this.auditoriumController = auditoriumController;
        this.eventController = eventController;
        this.errorDialog = errorDialog;
        this.eventsTableModel = eventsTableModel;
    }

    void display() {
        this.event = new Event();
        setUpFields(null);
        showIt("Új vetítés");
    }

    void display(Event event) {
        this.event = event;
        setUpFields(event);
        showIt(event.getMovie().getTitle() + " - " + event.getAuditorium().getName());
    }

    private void showIt(String title) {
        movieInput.setModel(new DefaultComboBoxModel<>(movieController.values()));
        auditoriumInput.setModel(new DefaultComboBoxModel<>(auditoriumController.values()));
        setTitle(title);
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    private void setUpButtons() {
        buttonOK.addActionListener(e -> {
            try {
                onOK();
            } catch (IllegalArgumentException exception) {
                errorDialog. showIt(exception.getMessage());
            } catch (ConstraintViolationException violation) {
                errorDialog.showIt(violation.getConstraintViolations().stream().map(ConstraintViolation::getMessage).collect(Collectors.joining(", ")));
            } catch (TransactionSystemException exception) {
                ConstraintViolationException violation = (ConstraintViolationException)exception.getCause().getCause();
                errorDialog.showIt(violation.getConstraintViolations().stream().map(ConstraintViolation::getMessage).collect(Collectors.joining(", ")));
            } catch (DateTimeException exception) {
                errorDialog.showIt(exception.getMessage());
            }
        });

        buttonCancel.addActionListener(e -> onCancel());
    }

    private void onOK() {
        DateModel<?> model = datePicker.getModel();
        event.setMovie((Movie) movieInput.getSelectedItem());
        event.setAuditorium((Auditorium) auditoriumInput.getSelectedItem());
        event.setStart(LocalDateTime.of(model.getYear(), model.getMonth()+1, model.getDay(),(Integer)hourInput.getValue(), (Integer) minuteInput.getValue()));
        eventController.add(event);
        eventsTableModel.fireTableDataChanged();
        dispose();
    }

    private void setUpFields(Event event) {
        if(event == null) {
            datePicker.getModel().setDate(2000, 1, 1);
            hourInput.setValue(0);
            minuteInput.setValue(0);
        } else {
            movieInput.setSelectedItem(event.getMovie());
            auditoriumInput.setSelectedItem(event.getAuditorium());
            datePicker.getModel().setDate(event.getStart().getYear(), event.getStart().getMonthValue(), event.getStart().getDayOfMonth());
            hourInput.setValue(event.getStart().getHour());
            minuteInput.setValue(event.getStart().getMinute());
        }
    }

    private void onCancel() {
        dispose();
    }

    private void createUIComponents() {
        UtilDateModel model = new UtilDateModel();
        Properties p = new Properties();
        p.put("text.today", "Today");
        p.put("text.month", "Month");
        p.put("text.year", "Year");
        JDatePanelImpl datePanel = new JDatePanelImpl(model, p);
        datePicker = new JDatePickerImpl(datePanel, new DateLabelFormatter());
    }

    private class DateLabelFormatter extends JFormattedTextField.AbstractFormatter {

        private String datePattern = "yyyy-MM-dd";
        private SimpleDateFormat dateFormatter = new SimpleDateFormat(datePattern);

        @Override
        public Object stringToValue(String text) throws ParseException {
            return dateFormatter.parseObject(text);
        }

        @Override
        public String valueToString(Object value) throws ParseException {
            if (value != null) {
                Calendar cal = (Calendar) value;
                return dateFormatter.format(cal.getTime());
            }

            return "";
        }

    }
}

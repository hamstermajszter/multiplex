package me.balazs.multiplex.view;

import me.balazs.multiplex.controller.AuditoriumController;
import me.balazs.multiplex.view.model.AuditoriumsTableModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;

@Component
public class AuditoriumsView {
    private JPanel innerPanel;
    private JButton addNew;
    private JButton deleteButton;
    private JTable table;
    private JButton editButton;

    @Autowired private AuditoriumsTableModel auditoriumsTableModel;

    @Autowired private AuditoriumController auditoriumController;

    @Autowired private AuditoriumDialog auditoriumDialog;

    @PostConstruct
    public void init(){
        setUpButtons();
        enableModificationButtons(false);
        setUpTable();
    }

    private void setUpTable() {
        table.setModel(auditoriumsTableModel);
        table.getSelectionModel().addListSelectionListener(e -> enableModificationButtons(true));
    }

    private void enableModificationButtons(boolean enable) {
        editButton.setEnabled(enable);
        deleteButton.setEnabled(enable);
    }

    private void setUpButtons() {
        addNew.addActionListener(e -> auditoriumDialog.display());
        deleteButton.addActionListener(e -> {
            auditoriumController.delete(auditoriumsTableModel.getValueAt(table.getSelectedRow()).getId());
            auditoriumsTableModel.fireTableDataChanged();
        });
        editButton.addActionListener(e -> auditoriumDialog.display(auditoriumsTableModel.getValueAt(table.getSelectedRow())));
    }

    JPanel getInnerPanel() {
        auditoriumsTableModel.fireTableDataChanged();
        return innerPanel;
    }

    public void onAddNew(){}
}

package me.balazs.multiplex.view;

import me.balazs.multiplex.controller.MovieController;
import me.balazs.multiplex.domain.Movie;
import me.balazs.multiplex.view.model.MoviesTableModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.persistence.Table;
import javax.swing.*;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

@Component
public class MoviesView {
    private JPanel innerPanel;
    private JButton addNewFilmButton;
    private JTable table;
    private JTextArea titleArea;
    private JTextArea originArea;
    private JTextArea lengthArea;
    private JTextArea maxPlayArea;
    private JTextArea ageCategoryArea;
    private JTextArea synopsisArea;
    private JTextArea dubbedArea;
    private JTextArea directorArea;
    private JButton deleteButton;
    private JButton editButton;

    @Autowired private MovieDialog movieDialog;

    @Autowired private MoviesTableModel moviesTableModel;

    @Autowired private MovieController movieController;

    @PostConstruct
    private void init() {
        setUpButtons();
        setUpTable();
    }

    private void setUpButtons() {
        addNewFilmButton.addActionListener(e -> movieDialog.display());
        deleteButton.addActionListener(e -> {
            movieController.delete(moviesTableModel.getValueAt(table.getSelectedRow()).getId());
            moviesTableModel.fireTableDataChanged();
        });
        editButton.addActionListener(e -> movieDialog.display(moviesTableModel.getValueAt(table.getSelectedRow())));
    }

    private void setUpTable() {
        TableRowSorter<TableModel> sorter = new TableRowSorter<>(table.getModel());
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        table.getSelectionModel().addListSelectionListener(e -> fillSidebar(moviesTableModel.getValueAt(table.convertRowIndexToModel(table.getSelectedRow()))));
        table.setModel(moviesTableModel);
        table.setAutoCreateRowSorter(true);
    }

    private void fillSidebar(Movie movie) {
        if(movie == null) {
            titleArea.setText("");
            originArea.setText("");
            lengthArea.setText("");
            maxPlayArea.setText("");
            ageCategoryArea.setText("");
            synopsisArea.setText("");
            dubbedArea.setText("");
            directorArea.setText("");
            enableModificationButtons(false);
        } else {
            titleArea.setText(movie.getTitle());
            originArea.setText(movie.getOrigin().getName());
            lengthArea.setText(movie.getLength().toString() + " perc");
            maxPlayArea.setText(movie.getMaxPlay().toString());
            ageCategoryArea.setText(movie.getAgeCategory().toString());
            synopsisArea.setText(movie.getSynopsis());
            dubbedArea.setText(movie.getIsDubbed() ? "Igen" : "Nem");
            directorArea.setText(movie.getDirector());
            enableModificationButtons(true);
        }
    }

    private void enableModificationButtons(boolean enable) {
        deleteButton.setEnabled(enable);
        editButton.setEnabled(enable);
    }

    JPanel getInnerPanel() {
        moviesTableModel.fireTableDataChanged();
        return innerPanel;
    }

}

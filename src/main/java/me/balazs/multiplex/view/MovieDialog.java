package me.balazs.multiplex.view;

import com.neovisionaries.i18n.CountryCode;
import me.balazs.multiplex.controller.MovieController;
import me.balazs.multiplex.domain.Movie;
import me.balazs.multiplex.view.model.MoviesTableModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionSystemException;

import javax.annotation.PostConstruct;
import javax.swing.*;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.stream.Collectors;

@Component
public class MovieDialog extends JDialog {
    private JPanel contentPane;
    private JTextField titleInput;
    private JTextField directorInput;
    private JTextArea synopsisInput;
    private JCheckBox isDubbedCheckBox;
    private JSpinner lengthInput;
    private JSpinner maxPlayInput;
    private JSlider ageCategoryInput;
    private JComboBox<CountryCode> originComboBox;
    private JButton buttonOK;
    private JButton buttonCancel;

    private Movie movie;
    private final MovieController movieController;
    private final MoviesTableModel moviesTableModel;
    private final ErrorDialog errorDialog;

    @Autowired
    public MovieDialog(ErrorDialog errorDialog, MovieController movieController, MoviesTableModel moviesTableModel) {
        this.errorDialog = errorDialog;
        this.movieController = movieController;
        this.moviesTableModel = moviesTableModel;
    }

    @PostConstruct
    public void inti() {
        setContentPane(contentPane);
        setUpButtons();
        originComboBox.setModel(new DefaultComboBoxModel<>(CountryCode.values()));
    }

    void display(Movie movie) {
        this.movie = movie;
        setUpFields(movie);
        showIt(movie.getTitle());
    }

    void display() {
        this.movie = new Movie();
        setUpFields(null);
        showIt("Új film");
    }

    private void showIt(String title) {
        setTitle(title);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
        setVisible(true);
    }

    private void setUpButtons() {
        buttonOK.addActionListener(e -> {
            try {
                onOK();
            } catch (ConstraintViolationException violation){
                errorDialog.showIt(violation.getConstraintViolations().stream().map(ConstraintViolation::getMessage).collect(Collectors.joining(", ")));
            } catch (TransactionSystemException exception) {
                ConstraintViolationException violation = (ConstraintViolationException)exception.getCause().getCause();
                errorDialog.showIt(violation.getConstraintViolations().stream().map(ConstraintViolation::getMessage).collect(Collectors.joining(", ")));
            }
        });
        buttonCancel.addActionListener(e -> onCancel());
    }

    private void onOK() {
        movie.setTitle(titleInput.getText());
        movie.setDirector(directorInput.getText());
        movie.setOrigin((CountryCode) originComboBox.getSelectedItem());
        movie.setSynopsis(synopsisInput.getText());
        movie.setIsDubbed(isDubbedCheckBox.isSelected());
        movie.setLength(((Integer) lengthInput.getValue()));
        movie.setMaxPlay((Integer) maxPlayInput.getValue());
        movie.setAgeCategory(ageCategoryInput.getValue());
        movieController.add(movie);
        moviesTableModel.fireTableDataChanged();
        setUpFields(null);
        dispose();
    }

    private void setUpFields(Movie movie) {
        if (movie == null) {
            titleInput.setText("");
            directorInput.setText("");
            synopsisInput.setText("");
            isDubbedCheckBox.setSelected(Boolean.FALSE);
            lengthInput.setValue(0);
            maxPlayInput.setValue(0);
            ageCategoryInput.setValue(3);
            originComboBox.setSelectedIndex(0);
        } else {
            titleInput.setText(movie.getTitle());
            directorInput.setText(movie.getDirector());
            synopsisInput.setText(movie.getSynopsis());
            isDubbedCheckBox.setSelected(movie.getIsDubbed());
            lengthInput.setValue(movie.getLength());
            maxPlayInput.setValue(movie.getMaxPlay());
            ageCategoryInput.setValue(movie.getAgeCategory());
            originComboBox.setSelectedItem(movie.getOrigin());
        }
    }

    private void onCancel() {
        dispose();
    }

    @Override
    public JPanel getContentPane() {
        return contentPane;
    }


}

package me.balazs.multiplex.view;

import org.springframework.stereotype.Component;

import javax.swing.*;

@Component
public class ErrorDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JLabel errorMessage;

    public ErrorDialog() {
        setContentPane(contentPane);
        buttonOK.addActionListener(e -> onCancel());
    }

    void showIt(String errorMessage) {
        setTitle("Hiba");
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.errorMessage.setText(errorMessage);
        this.pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    private void onCancel() {
        dispose();
    }
}

package me.balazs.multiplex.view;

import me.balazs.multiplex.controller.EventController;
import me.balazs.multiplex.controller.TicketController;
import me.balazs.multiplex.domain.Event;
import me.balazs.multiplex.domain.Ticket;
import me.balazs.multiplex.domain.TicketStatus;
import me.balazs.multiplex.view.model.TicketTableModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionSystemException;

import javax.swing.*;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.stream.Collectors;

@Component
public class TicketDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JComboBox<Event> eventInput;
    private JSpinner rowInput;
    private JSpinner columnInput;

    private Ticket ticket;

    @Autowired private TicketController ticketController;

    @Autowired private TicketTableModel ticketTableModel;

    @Autowired private EventController eventController;

    @Autowired private ErrorDialog errorDialog;

    public TicketDialog() {
        setContentPane(contentPane);
        setUpButtons();
    }

    void display() {
        this.ticket = new Ticket();
        showIt("Új jegy");
        setUpFields(null);
    }

    void display(Ticket ticket) {
        this.ticket = ticket;
        showIt(ticket.getEvent().getMovie().getTitle() + ", " + ticket.getEvent().getAuditorium().getName() + ", sor: " + ticket.getSeatRow() + " oszlop:" + ticket.getSeatColumn());
        setUpFields(ticket);
    }

    private void showIt(String title) {
        eventInput.setModel(new DefaultComboBoxModel<>(eventController.values()));
        setTitle(title);
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    private void setUpButtons() {
        buttonOK.addActionListener(e -> {
            try {
                onOK();
            }  catch (ConstraintViolationException violation) {
                errorDialog.showIt(violation.getConstraintViolations().stream().map(ConstraintViolation::getMessage).collect(Collectors.joining(", ")));
            } catch (TransactionSystemException exception) {
                ConstraintViolationException violation = (ConstraintViolationException)exception.getCause().getCause();
                errorDialog.showIt(violation.getConstraintViolations().stream().map(ConstraintViolation::getMessage).collect(Collectors.joining(", ")));
            } catch (IllegalArgumentException exception) {
                errorDialog.showIt(exception.getMessage());
            }
        });

        buttonCancel.addActionListener(e -> onCancel());
    }

    private void onOK() {
        ticket.setEvent((Event)eventInput.getSelectedItem());
        ticket.setSeatRow((Integer)rowInput.getValue());
        ticket.setSeatColumn((Integer)columnInput.getValue());
        ticket.setTicketStatus(TicketStatus.RESERVED);
        ticketController.add(ticket);
        ticketTableModel.fireTableDataChanged();
        dispose();
    }

    private void setUpFields(Ticket ticket) {
        if(ticket == null) {
            eventInput.setSelectedIndex(0);
            rowInput.setValue(0);
            columnInput.setValue(0);
        } else {
            eventInput.setSelectedItem(ticket.getEvent());
            rowInput.setValue(ticket.getSeatRow());
            columnInput.setValue(ticket.getSeatColumn());
        }
    }

    private void onCancel() {
        dispose();
    }

    @Override
    public JPanel getContentPane() {
        return contentPane;
    }
}

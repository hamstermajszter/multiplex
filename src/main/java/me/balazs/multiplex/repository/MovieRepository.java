package me.balazs.multiplex.repository;

import me.balazs.multiplex.domain.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long>{

    List<Movie> findByTitle (String title);
    Movie findOne (Long id);

}

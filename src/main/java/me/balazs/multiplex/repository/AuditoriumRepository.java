package me.balazs.multiplex.repository;

import me.balazs.multiplex.domain.Auditorium;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AuditoriumRepository extends JpaRepository<Auditorium, Long>{

    List<Auditorium> findByName(String name);
    Auditorium findOne(Long id);

}

package me.balazs.multiplex.repository;

import me.balazs.multiplex.domain.Auditorium;
import me.balazs.multiplex.domain.Event;
import me.balazs.multiplex.domain.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EventRepository extends JpaRepository <Event, Long >{

    List<Event> findByMovie(Movie movie);
    List<Event> findByAuditorium(Auditorium auditorium);
    List<Event> findByMovieAndAuditorium(Movie movie, Auditorium auditorium);

}

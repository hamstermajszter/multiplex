package me.balazs.multiplex.repository;

import me.balazs.multiplex.domain.Ticket;
import me.balazs.multiplex.domain.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface TicketRepository extends JpaRepository <Ticket, Long> {

    List<Ticket> findByEvent(Event event);
    List<Ticket> findByEventIn(Collection<Event> events);

}

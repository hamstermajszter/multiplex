package me.balazs.multiplex.controller;

import me.balazs.multiplex.domain.Auditorium;

import java.time.LocalDateTime;
import java.util.List;

public interface AuditoriumController {

    void add(Auditorium auditorium);
    void delete(Long id);
    List<Auditorium> getAll();
    Auditorium[] values();
    Auditorium getById(Long id);
    boolean isAvailable(Auditorium auditorium, LocalDateTime start, LocalDateTime end);

    int getAmountOfSeats(Auditorium auditorium);
}

package me.balazs.multiplex.controller;

import me.balazs.multiplex.domain.Movie;

import java.util.List;

public interface MovieController {

    void add(Movie movie);
    void delete(Long id);
    List<Movie> getAll();
    Movie[] values();
    Movie getById(Long id);
    int getAmountOfScreenings(Movie movie);
    int getAmountOfSoldTickets(Movie movie);

}

package me.balazs.multiplex.controller;

import me.balazs.multiplex.domain.Event;
import me.balazs.multiplex.domain.Ticket;
import me.balazs.multiplex.domain.TicketStatus;
import me.balazs.multiplex.repository.TicketRepository;
import me.balazs.multiplex.util.validator.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class DefaultTicketController implements TicketController {

    @Autowired private TicketRepository ticketRepository;
    @Autowired private EventController eventController;
    @Autowired @Qualifier("ticketValidator") private Validator validator;

    @Override
    public void add(Ticket ticket) {
        ticket.setTicketStatus(TicketStatus.RESERVED);
        validator.validate(ticket);
        ticketRepository.save(ticket);
    }

    @Override
    public void sellTicket(Ticket ticket) {
        ticket.setTicketStatus(TicketStatus.BOUGHT);
        ticketRepository.save(ticket);
    }

    @Override
    public void deleteTicket(Ticket ticket) {
        ticketRepository.delete(ticket.getId());
    }

    @Override
    public List<Ticket> getAll() {
        return ticketRepository.findAll();
    }

    @Override
    public List<Ticket> getByEvent(Event event) {
        return ticketRepository.findByEvent(event);
    }

    @Override
    public Ticket getById(long id) {
        return ticketRepository.findOne(id);
    }

    @Override
    public List<Ticket> getByEvents(List<Event> events) {
        return ticketRepository.findByEventIn(events);
    }
}

package me.balazs.multiplex.controller;

import me.balazs.multiplex.domain.Auditorium;
import me.balazs.multiplex.domain.Event;
import me.balazs.multiplex.repository.AuditoriumRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.util.List;

public class DefaultAuditoriumController implements AuditoriumController {

    @Autowired private AuditoriumRepository auditoriumRepository;
    @Autowired private EventController eventController;

    @Override
    public void add(Auditorium auditorium) {
        auditoriumRepository.save(auditorium);
    }

    @Override
    public void delete(Long id) {
        auditoriumRepository.delete(id);
    }

    @Override
    public List<Auditorium> getAll() {
        return auditoriumRepository.findAll();
    }

    @Override
    public Auditorium[] values() {
        List<Auditorium> auditoriums = getAll();
        return auditoriums.toArray(new Auditorium[auditoriums.size()]);
    }

    @Override
    public Auditorium getById(Long id) { return auditoriumRepository.findOne(id); }

    @Override
    public boolean isAvailable(Auditorium auditorium, LocalDateTime start, LocalDateTime end) {
        List<Event> events = eventController.getByAuditorium(auditorium);
        for(Event event : events){
            if(eventController.isDuringTheEvent(event, start, end)){
                return false;
            }
        }
        return true;
    }

    @Override
    public int getAmountOfSeats(Auditorium auditorium) {
        return auditorium.getRowSize() * auditorium.getColumnSize();
    }



}


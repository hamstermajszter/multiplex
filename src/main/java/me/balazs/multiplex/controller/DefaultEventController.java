package me.balazs.multiplex.controller;

import me.balazs.multiplex.domain.Auditorium;
import me.balazs.multiplex.domain.Event;
import me.balazs.multiplex.domain.Movie;
import me.balazs.multiplex.domain.Ticket;
import me.balazs.multiplex.repository.EventRepository;
import me.balazs.multiplex.util.validator.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.validation.ConstraintViolationException;
import java.time.LocalDateTime;
import java.util.List;

public class DefaultEventController implements EventController{

    @Autowired @Qualifier("eventValidator") private Validator eventValidator;
    @Autowired private EventRepository eventRepository;
    @Autowired private AuditoriumController auditoriumController;
    @Autowired private MovieController movieController;
    @Autowired private TicketController ticketController;

    @Override
    public void add(Event event) throws ConstraintViolationException {
        eventValidator.validate(event);
        eventRepository.save(event);
    }

    @Override
    public void delete(Long id) {
        eventRepository.delete(id);
    }

    @Override
    public List<Event> getAll() {
        return eventRepository.findAll();
    }

    @Override
    public List<Event> getByMovie(Movie movie) {
        return eventRepository.findByMovie(movie);
    }

    @Override
    public List<Event> getByAuditorium(Auditorium auditorium) {
        return eventRepository.findByAuditorium(auditorium);
    }

    @Override
    public Event get(Long id) {
        return eventRepository.findOne(id);
    }

    @Override
    public List<Ticket> getTickets(Event event) {
        return ticketController.getByEvent(event);
    }

    @Override
    public int getAmountOfFreeSeats(Event event) {
        return auditoriumController.getAmountOfSeats(event.getAuditorium()) - getAmountOfTickets(event);
    }

    @Override
    public int getAmountOfTickets(Event event) {
        return getTickets(event).size();
    }

    @Override
    public boolean isSeatIsFree(Event event, int rowIndex, int columnIndex) {
        List<Ticket> tickets = getTickets(event);
        System.out.println(tickets.size());
        for (Ticket ticket : tickets) {
            if(ticket.getSeatColumn() == columnIndex && ticket.getSeatRow() == rowIndex){
                return false;
            }
        }
        return true;
    }

    @Override
    public Event[] values() {
        List<Event> events = getAll();
        return events.toArray(new Event[events.size()]);
    }

    @Override
    public boolean isDuringTheEvent(Event event, LocalDateTime start, LocalDateTime end) {
        return isDuringTheEvent(event, start) || isDuringTheEvent(event, end) || (start.isBefore(event.getStart()) && end.isAfter(event.getEnd())) ;
    }

    @Override
    public boolean isEventsSimultaneous(Event event1, Event event2) {
        return event1 == event2 || isDuringTheEvent(event1, event2.getStart(), event2.getEnd());
    }

    public boolean isDuringTheEvent(Event event, LocalDateTime time){
        return !time.isBefore(event.getStart()) && !time.isAfter(event.getEnd());
    }
}

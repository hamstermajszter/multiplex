package me.balazs.multiplex.controller;

import me.balazs.multiplex.domain.Event;
import me.balazs.multiplex.domain.Movie;
import me.balazs.multiplex.domain.Ticket;
import me.balazs.multiplex.domain.TicketStatus;
import me.balazs.multiplex.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;
import java.util.stream.Collectors;

@Controller
public class DefaultMovieController implements MovieController {

    @Autowired private MovieRepository movieRepository;
    @Autowired private EventController eventController;
    @Autowired private TicketController ticketController;


    @Override
    public void add(Movie movie) { movieRepository.save(movie); }

    @Override
    public void delete(Long id) {
        movieRepository.delete(id);
    }

    @Override
    public List<Movie> getAll() { return movieRepository.findAll(); }

    @Override
    public Movie[] values() {
        List<Movie> movies = getAll();
        return movies.toArray(new Movie[movies.size()]);
    }

    @Override
    public Movie getById(Long id) { return movieRepository.findOne(id);}

    @Override
    public int getAmountOfScreenings(Movie movie) {
        return eventController.getByMovie(movie).size();
    }

    @Override
    public int getAmountOfSoldTickets(Movie movie) {
        List<Event> eventsByMovie = eventController.getByMovie(movie);
        List<Ticket> ticketsForMovie = ticketController.getByEvents(eventsByMovie);
        return ticketsForMovie.stream().filter(ticket -> ticket.getTicketStatus().equals(TicketStatus.BOUGHT)).collect(Collectors.toList()).size();
    }

}

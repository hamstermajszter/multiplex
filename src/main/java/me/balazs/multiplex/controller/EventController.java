package me.balazs.multiplex.controller;

import me.balazs.multiplex.domain.Auditorium;
import me.balazs.multiplex.domain.Event;
import me.balazs.multiplex.domain.Movie;
import me.balazs.multiplex.domain.Ticket;

import javax.validation.ConstraintViolationException;
import java.time.LocalDateTime;
import java.util.List;

public interface EventController {

    void add(Event event) throws ConstraintViolationException;
    void delete(Long id);
    Event get(Long id);
    Event[] values();
    List<Event> getAll();
    List<Event> getByMovie(Movie movie);
    List<Event> getByAuditorium(Auditorium auditorium);
    List<Ticket> getTickets(Event event);
    int getAmountOfFreeSeats(Event event);
    int getAmountOfTickets(Event event);
    boolean isSeatIsFree(Event evet, int rowIndex, int columnIndex);
    boolean isDuringTheEvent(Event event, LocalDateTime start, LocalDateTime end);
    boolean isEventsSimultaneous(Event event1, Event event2);
}

package me.balazs.multiplex.controller;

import me.balazs.multiplex.domain.Event;
import me.balazs.multiplex.domain.Ticket;

import java.util.List;

public interface TicketController {

    void add(Ticket ticket);
    void sellTicket(Ticket ticket);
    void deleteTicket(Ticket ticket);
    Ticket getById(long id);
    List<Ticket> getAll();
    List<Ticket> getByEvent(Event event);
    List<Ticket> getByEvents(List<Event> events);
}

package me.balazs.multiplex.domain;

import com.neovisionaries.i18n.CountryCode;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@Entity
@Table(name="movies")
@Builder
public class Movie implements Serializable{

    @Id @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

	@NotNull private String title;
    @NotNull private CountryCode origin;
    @NotNull private Boolean isDubbed;
    @NotNull private String director;
    @NotNull private String synopsis;
    @Min(value = 1, message = "A hossznak nagyobbnak kell lennie 0-nál") private Integer length;
    @Min(value = 1, message = "A maximális lejátszások száma nem lehet 1-nél kisebb") private Integer maxPlay;
    @Min(1) @Max(5) private Integer ageCategory;

    @Tolerate
    public Movie() {}

    @Override
    public String toString(){
        return director + ": " + title;
    }

}

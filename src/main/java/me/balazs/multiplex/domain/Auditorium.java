package me.balazs.multiplex.domain;

import lombok.*;
import lombok.experimental.Tolerate;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Builder
@Data
@Entity
@Table(name="aditoriums")
public class Auditorium implements Serializable {

    @Tolerate
    public Auditorium() {}

    @Id @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

	@NotNull private String name;
    @Min(value = 1, message = "A sorok számának nagyobbnak kell lennie 0-nál") private int rowSize;
    @Min(value = 1, message = "Az oszlopok számának nagyobbnak kell lennie 0-nál") private int columnSize;

    @Override
    public String toString(){
        return name;
    }

}

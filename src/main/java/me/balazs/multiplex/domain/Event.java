package me.balazs.multiplex.domain;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

@Builder
@Data
@Entity
@Table(name="events")
public class Event implements Serializable {

    private static int CLEAN_TIME = 30;

    @Id @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @NotNull private Movie movie;
    @OneToOne
    @NotNull private Auditorium auditorium;
    @NotNull private LocalDateTime start;

    @Tolerate
    public Event(){}

    public LocalDateTime getEnd() {
        return start.plusMinutes(movie.getLength() + CLEAN_TIME) ;
    }

    @Override
    public String toString() {
        return movie.toString() + " - " + auditorium.toString();
    }
}

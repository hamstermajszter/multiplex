package me.balazs.multiplex.domain;

public enum TicketStatus {
    RESERVED, BOUGHT
}

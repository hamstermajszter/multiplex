package me.balazs.multiplex.domain;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Builder
@Entity
@Data
@Table(name="tickets")
public class Ticket {

    @Id @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @NotNull private Event event;
    @Min(1) private Integer seatRow;
    @Min(1) private Integer seatColumn;
    @NotNull @Enumerated(EnumType.STRING) private TicketStatus ticketStatus;

    @Tolerate
    public Ticket(){}

    public Auditorium getAudotorium(){
        return event.getAuditorium();
    }
    public Movie getMovie() { return event.getMovie(); }
    public LocalDateTime getStart() { return event.getStart(); }

}

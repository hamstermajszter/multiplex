package me.balazs.multiplex.util;

import com.neovisionaries.i18n.CountryCode;
import me.balazs.multiplex.domain.*;
import me.balazs.multiplex.repository.AuditoriumRepository;
import me.balazs.multiplex.repository.EventRepository;
import me.balazs.multiplex.repository.MovieRepository;
import me.balazs.multiplex.repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class Populator {

    @Autowired private MovieRepository movieRepository;
    @Autowired private AuditoriumRepository auditoriumRepository;
    @Autowired private EventRepository eventRepository;
    @Autowired private TicketRepository ticketRepository;

    public void populate(){
        Movie terminator =  Movie.builder().title("Terminator").director("Cameron").isDubbed(Boolean.TRUE).length(120).synopsis("Ez egy jó film").maxPlay(5).ageCategory(5).origin(CountryCode.US).build();
        Movie avatar =      Movie.builder().title("Avatar").director("Cameron").isDubbed(Boolean.TRUE).length(100).synopsis("Ez annyira nem jó film").maxPlay(2).ageCategory(1).origin(CountryCode.US).build();
        Movie drStrange =   Movie.builder().title("Dr. Strange").director("Scott Derrickson").isDubbed(Boolean.FALSE).length(130).synopsis("Ez egy új film film").maxPlay(30).ageCategory(4).origin(CountryCode.US).build();

        Auditorium petofi = Auditorium.builder().name("Petőfi").columnSize(2).rowSize(3).build();
        Auditorium arany =  Auditorium.builder().name("Arany").columnSize(5).rowSize(5).build();
        Auditorium jozsef = Auditorium.builder().name("József").columnSize(1).rowSize(1).build();
        Auditorium ady =    Auditorium.builder().name("Ady").columnSize(50).rowSize(50).build();

        Event terminatorPetofi =    Event.builder().movie(terminator).auditorium(petofi).start(LocalDateTime.of(2016,10,12,21,15)).build();
        Event terminatorArany =     Event.builder().auditorium(arany).movie(terminator).start(LocalDateTime.of(2016,10,12,21,45)).build();
        Event terminatorAdy =       Event.builder().auditorium(ady).movie(terminator).start(LocalDateTime.of(2016,10,12,21,50)).build();
        Event avatarPetofi =        Event.builder().auditorium(petofi).movie(avatar).start(LocalDateTime.of(2016,10,12,10,50)).build();

        Ticket terminatorPetofi_1_2 =   Ticket.builder().event(terminatorPetofi).seatColumn(1).seatRow(2).ticketStatus(TicketStatus.RESERVED).build();
        Ticket terminatorPetofi_2_2 =   Ticket.builder().event(terminatorPetofi).seatColumn(2).seatRow(2).ticketStatus(TicketStatus.RESERVED).build();
        Ticket terminatorPetofi_1_3 =   Ticket.builder().event(terminatorPetofi).seatColumn(1).seatRow(3).ticketStatus(TicketStatus.RESERVED).build();
        Ticket terminatorArany_4_4 =    Ticket.builder().event(terminatorArany).seatColumn(4).seatRow(3).ticketStatus(TicketStatus.RESERVED).build();

        movieRepository.save(terminator);
        movieRepository.save(avatar);
        movieRepository.save(drStrange);

        auditoriumRepository.save(petofi);
        auditoriumRepository.save(arany);
        auditoriumRepository.save(jozsef);
        auditoriumRepository.save(ady);

        eventRepository.save(terminatorPetofi);
        eventRepository.save(terminatorArany);
        eventRepository.save(terminatorAdy);
        eventRepository.save(avatarPetofi);

        ticketRepository.save(terminatorPetofi_1_2);
        ticketRepository.save(terminatorPetofi_2_2);
        ticketRepository.save(terminatorPetofi_1_3);
        ticketRepository.save(terminatorArany_4_4);
    }
}

package me.balazs.multiplex.util.validator;

import me.balazs.multiplex.domain.Event;

public class AgeCategoryRule implements Rule<Event> {

    @Override
    public void validate(Event event) {
        if(event.getMovie().getAgeCategory() == 4 && event.getStart().getHour()<17) {
            throw new IllegalArgumentException("Nem lehet 17 óra előtt 4-es besorolású filmet vetíteni");
        }
        if(event.getMovie().getAgeCategory() == 5 && event.getStart().getHour()<21) {
            throw new IllegalArgumentException("Nem lehet 21 óra előtt 5-es besorolású filmet vetíteni");
        }
    }
}

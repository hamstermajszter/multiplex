package me.balazs.multiplex.util.validator;

import me.balazs.multiplex.controller.AuditoriumController;
import me.balazs.multiplex.domain.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.DateTimeException;

@Component
public class AuditoriumIsFreeRule implements Rule<Event> {

    @Autowired private AuditoriumController auditoriumController;

    @Override
    public void validate(Event event) {
        if(!auditoriumController.isAvailable(event.getAuditorium(),event.getStart(),event.getEnd())) {
            throw new DateTimeException("A terem foglalt az adott időpontban");
        }
    }
}

package me.balazs.multiplex.util.validator;

public interface Rule<E> {

    void validate(E entity);

}

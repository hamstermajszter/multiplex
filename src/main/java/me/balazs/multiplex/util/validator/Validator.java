package me.balazs.multiplex.util.validator;

import java.util.ArrayList;
import java.util.List;
import me.balazs.multiplex.domain.Event;
import org.springframework.stereotype.Component;

@Component
public class Validator<E> {

    private List<Rule<E>> rules = new ArrayList<>();

    public void addRule(Rule rule) {
        rules.add(rule);
    }

    public void validate(E event){
        rules.forEach(rule -> rule.validate(event));
    }

}

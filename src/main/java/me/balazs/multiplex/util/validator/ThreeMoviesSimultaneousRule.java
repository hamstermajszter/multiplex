package me.balazs.multiplex.util.validator;

import me.balazs.multiplex.controller.EventController;
import me.balazs.multiplex.domain.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Component
public class ThreeMoviesSimultaneousRule implements Rule<Event> {

    @Autowired private EventController eventController;

    @Override
    public void validate(Event underValidation) {
        List<Event> eventsByMovie = eventController.getByMovie(underValidation.getMovie());
        List<Event> filteredEvents = eventsByMovie.stream().filter(e -> eventController.isEventsSimultaneous(e, underValidation)).collect(Collectors.toList());
        filteredEvents.forEach(event -> {
            Predicate<Event> simultaneousEvents = e -> eventController.isEventsSimultaneous(event, e);
            if(filteredEvents.stream().filter(simultaneousEvents).count() > 2) {
                throw new IllegalArgumentException("Nem futhat ugyan abból a filmből 3-nál több egy időben");
            }
        });
    }
}

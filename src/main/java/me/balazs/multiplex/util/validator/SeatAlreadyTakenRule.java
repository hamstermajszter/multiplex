package me.balazs.multiplex.util.validator;

import me.balazs.multiplex.controller.EventController;
import me.balazs.multiplex.domain.Ticket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SeatAlreadyTakenRule implements Rule<Ticket> {

    @Autowired private EventController eventController;

    @Override
    public void validate(Ticket ticket) {
        if(!eventController.isSeatIsFree(ticket.getEvent(), ticket.getSeatRow(), ticket.getSeatColumn())){
            throw new IllegalArgumentException("A hely már foglalt");
        }
    }
}

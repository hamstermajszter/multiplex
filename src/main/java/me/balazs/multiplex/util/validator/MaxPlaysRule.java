package me.balazs.multiplex.util.validator;

import me.balazs.multiplex.controller.MovieController;
import me.balazs.multiplex.domain.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MaxPlaysRule implements Rule<Event> {

    @Autowired private MovieController movieController;

    @Override
    public void validate(Event event) throws IllegalArgumentException {
        int amountOfScreenings = movieController.getAmountOfScreenings(event.getMovie());
        if(amountOfScreenings >= event.getMovie().getMaxPlay()){
            throw new IllegalArgumentException("A film már elérte a maximális vetítésszámot");
        }
    }
}
